
contract hashbet {

    uint constant s_open               = 0; 
    uint constant s_wait               = 1; 
    uint constant s_canceled           = 2; 
    uint constant s_resolved_creator   = 3; 
    uint constant s_resolved_buyer     = 4; 

    uint constant d_down               = 0;
    uint constant d_up                 = 1;

    address owner;

    function hashbet() internal {
        owner = msg.sender;
    }

  struct EntityStruct {
    address creator;
    address buyer;

    uint    t_block;
    uint    t_diff;
    uint    status;
    uint    stake;
    uint    price;
    uint    direction;

    uint b_open;
    uint b_cancel;
    uint b_buy;
    uint b_resolve;

    address resolver;
  }

  EntityStruct[] public entityStructs;
  function open (
                  uint t_block   ,
                  uint t_diff    ,
                  uint price     ,
                  uint direction     
              ) public payable returns(uint rowNumber) {
                  if (msg.value < 0.01 ether  ) { throw; } 
                  if (    price < 0.01 ether  ) { throw; } 
                  if (  t_block < block.number) { throw; } 

                  EntityStruct memory newEntity;

                  newEntity.creator    = msg.sender ;

                  newEntity.t_block    = t_block    ;
                  newEntity.t_diff     = t_diff     ;
                  newEntity.status     = s_open     ;
                  newEntity.price      = price      ;
                  newEntity.stake      = msg.value  ;

                  if (direction == 0) {
                      newEntity.direction  = d_down;
                  } else {
                      newEntity.direction  = d_up  ;
                  }

                  newEntity.b_open  = block.number;

                  return entityStructs.push(newEntity);
  }

    function cancel (
        uint i
    ) public payable returns(uint rowNumber) {
        if (msg.sender              != entityStructs[i].creator )    { throw; } 
        if (entityStructs[i].status != s_open                   )    { throw; } 

        entityStructs[i].status    = s_canceled  ;
        entityStructs[i].b_cancel  = block.number;

        (entityStructs[i].creator).send(entityStructs[i].stake);
        return i;
    }
    function buy (
        uint i
    ) public payable returns(uint rowNumber) {
        if (msg.value               != entityStructs[i].price) { throw; } 
        if (entityStructs[i].status != s_open                ) { throw; } 

        entityStructs[i].buyer  = msg.sender  ;
        entityStructs[i].status = s_wait      ;
        entityStructs[i].b_buy  = block.number;

        return i;
    }

    function distance( uint a, uint b )public returns (uint c){
        if (a>b){ return a-b; }
        else    { return b-a; }
    }

    function de_resolve (
        uint i
    ) public payable returns(uint rowNumber) {

        if (         entityStructs[i].status               != s_wait)                 { throw; }
        if (distance(entityStructs[i].t_block,block.number) > 10    )                 { throw; }

        uint total = entityStructs[i].stake +
                     entityStructs[i].price   ;

        uint fee = ( total / 10000 ) * 25 ;
        uint remains = total-fee;

        msg.sender.send(fee/2);
             owner.send(fee/2);

        if (entityStructs[i].direction == d_up){
            if(diff_fn() >= entityStructs[i].t_diff) {
                (entityStructs[i].creator).send(remains);
                 entityStructs[i].status = s_resolved_creator;
            }else {
                (entityStructs[i].buyer  ).send(remains);
                 entityStructs[i].status = s_resolved_buyer;
            }
        }
        if (entityStructs[i].direction == d_down){
            if(diff_fn() <= entityStructs[i].t_diff) {
                (entityStructs[i].creator).send(remains);
                 entityStructs[i].status = s_resolved_creator;
            }else {
                (entityStructs[i].buyer  ).send(remains);
                 entityStructs[i].status = s_resolved_buyer;
            }
        }
        entityStructs[i].b_resolve  = block.number;
        entityStructs[i].resolver   = msg.sender;
        return i;
    }

    function ce_resolve (
        uint i,
        uint diff
    ) public payable returns(uint rowNumber) {

        if ( msg.sender               != owner        )  { throw; } 
        if ( entityStructs[i].status  != s_wait       )  { throw; }
        if ( entityStructs[i].t_block  > block.number )  { throw; }

        uint total = entityStructs[i].stake +
                     entityStructs[i].price   ;

        uint fee = ( total / 10000 ) * 25 ;
        uint remains = total-fee;

             owner.send(fee);

        if (entityStructs[i].direction == d_up){
            if(diff_fn() >= entityStructs[i].t_diff) {
                (entityStructs[i].creator).send(remains);
                 entityStructs[i].status = s_resolved_creator;
            }else {
                (entityStructs[i].buyer  ).send(remains);
                 entityStructs[i].status = s_resolved_buyer;
            }
        }
        if (entityStructs[i].direction == d_down){
            if(diff_fn() <= entityStructs[i].t_diff) {
                (entityStructs[i].creator).send(remains);
                 entityStructs[i].status = s_resolved_creator;
            }else {
                (entityStructs[i].buyer  ).send(remains);
                 entityStructs[i].status = s_resolved_buyer;
            }
        }

        entityStructs[i].b_resolve  = block.number;
        entityStructs[i].resolver   = owner;
        return i;
    }

    function beti(uint i) public constant returns(address, address, uint, uint, uint, uint ) {
      return (
          entityStructs[i].creator ,
          entityStructs[i].buyer   ,
          entityStructs[i].t_block ,
          entityStructs[i].t_diff  ,
          entityStructs[i].stake   ,
          entityStructs[i].price
      );
    }
    function betI(uint i) public constant returns( uint, uint, uint, uint, uint, uint ,address) {
      return (
          entityStructs[i].status   ,
          entityStructs[i].direction,
          entityStructs[i].b_open   ,
          entityStructs[i].b_cancel ,
          entityStructs[i].b_buy    ,
          entityStructs[i].b_resolve,
          entityStructs[i].resolver
      );
    }
    function length() public constant returns( uint ) {
      return (entityStructs.length);
    }
    function diff_fn() public constant returns( uint ) {
        return block.difficulty;
    }
 }

