

// result.contracts.hashbet.bytecode
// contract_bytecode = '606060405234610000575b60008054600160a060020a0319166c01000000000000000000000000338102041790555b5b6112138061003d6000396000f36060604052361561008d5760e060020a600035046318d6676c81146100925780631f7b6d321461011857806340e58ee514610137578063685f9854146101545780637366bd78146101715780639934c9fa146101915780639f82ad8c146101e2578063ce89f06314610208578063d96a094a1461022d578063e07352ef1461024a578063e679ef7114610299575b610000565b34610000576100a26004356102b8565b60408051600160a060020a039e8f1681529c8e1660208e01528c81019b909b5260608c019990995260808b019790975260a08a019590955260c089019390935260e08801919091526101008701526101208601526101408501526101608401529092166101808201529051908190036101a00190f35b346100005761012561033d565b60408051918252519081900360200190f35b610125600435610344565b60408051918252519081900360200190f35b610125600435610469565b60408051918252519081900360200190f35b610125600435602435610862565b60408051918252519081900360200190f35b34610000576101a1600435610c50565b604080519788526020880196909652868601949094526060860192909252608085015260a0840152600160a060020a031660c0830152519081900360e00190f35b610125600435602435604435606435610d72565b60408051918252519081900360200190f35b3461000057610125600435602435610ff0565b60408051918252519081900360200190f35b610125600435611013565b60408051918252519081900360200190f35b346100005761025a600435611108565b60408051600160a060020a039788168152959096166020860152848601939093526060840191909152608083015260a082015290519081900360c00190f35b346100005761012561120e565b60408051918252519081900360200190f35b60018181548110156100005790600052602060002090600d020160005b5080546001820154600283015460038401546004850154600586015460068701546007880154600889015460098a0154600a8b0154600b8c0154600c909c0154600160a060020a039b8c169d50998b169b989a9799969895979496939592949193909291168d565b6001545b90565b600060018281548110156100005790600052602060002090600d020160005b505433600160a060020a0390811691161461037d57610000565b600060018381548110156100005790600052602060002090600d020160005b5060040154146103ab57610000565b600260018381548110156100005790600052602060002090600d020160005b50600401819055504360018381548110156100005790600052602060002090600d020160005b50600901556001805483908110156100005790600052602060002090600d020160005b505460018054600160a060020a03909216916108fc919085908110156100005790600052602060002090600d020160005b50600501546040518115909202916000818181858888f150859450505050505b919050565b6000600060006000600160018681548110156100005790600052602060002090600d020160005b50600401541461049f57610000565b600a6104cc60018781548110156100005790600052602060002090600d020160005b506002015443610ff0565b11156104d757610000565b60018581548110156100005790600052602060002090600d020160005b506006015460018681548110156100005790600052602060002090600d020160005b50600501546040519101935061271084046019029250828403915033600160a060020a0316906002840480156108fc02916000818181858888f1505060008054604051600160a060020a0390911694506002870480156108fc029450925090818181858888f1935050505050600160018681548110156100005790600052602060002090600d020160005b506007015414156106b85760018581548110156100005790600052602060002090600d020160005b50600301546105d661120e565b1061064a5760018581548110156100005790600052602060002090600d020160005b5054604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600360018681548110156100005790600052602060002090600d020160005b50600401556106b8565b60018581548110156100005790600052602060002090600d020160005b5060010154604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600460018681548110156100005790600052602060002090600d020160005b50600401555b5b600060018681548110156100005790600052602060002090600d020160005b506007015414156107ef5760018581548110156100005790600052602060002090600d020160005b506003015461070d61120e565b116107815760018581548110156100005790600052602060002090600d020160005b5054604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600360018681548110156100005790600052602060002090600d020160005b50600401556107ef565b60018581548110156100005790600052602060002090600d020160005b5060010154604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600460018681548110156100005790600052602060002090600d020160005b50600401555b5b4360018681548110156100005790600052602060002090600d020160005b50600b01819055503360018681548110156100005790600052602060002090600d020160005b50600c018054600160a060020a031916606060020a928302929092049190911790558493505b505050919050565b6000805481908190819033600160a060020a0390811691161461088457610000565b600160018781548110156100005790600052602060002090600d020160005b5060040154146108b257610000565b4360018781548110156100005790600052602060002090600d020160005b506002015411156108e057610000565b60018681548110156100005790600052602060002090600d020160005b506006015460018781548110156100005790600052602060002090600d020160005b506005015460008054604051939092019550612710860460190294508486039350600160a060020a03909116916108fc85150291859190818181858888f1935050505050600160018781548110156100005790600052602060002090600d020160005b50600701541415610a995760018681548110156100005790600052602060002090600d020160005b50600301546109b761120e565b10610a2b5760018681548110156100005790600052602060002090600d020160005b5054604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600360018781548110156100005790600052602060002090600d020160005b5060040155610a99565b60018681548110156100005790600052602060002090600d020160005b5060010154604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600460018781548110156100005790600052602060002090600d020160005b50600401555b5b600060018781548110156100005790600052602060002090600d020160005b50600701541415610bd05760018681548110156100005790600052602060002090600d020160005b5060030154610aee61120e565b11610b625760018681548110156100005790600052602060002090600d020160005b5054604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600360018781548110156100005790600052602060002090600d020160005b5060040155610bd0565b60018681548110156100005790600052602060002090600d020160005b5060010154604051600160a060020a039091169082156108fc029083906000818181858888f1935050505050600460018781548110156100005790600052602060002090600d020160005b50600401555b5b4360018781548110156100005790600052602060002090600d020160005b50600b015560005460018054600160a060020a039092169188908110156100005790600052602060002090600d020160005b50600c018054600160a060020a031916606060020a928302929092049190911790558593505b50505092915050565b600060006000600060006000600060018881548110156100005790600052602060002090600d020160005b506004015460018981548110156100005790600052602060002090600d020160005b506007015460018a81548110156100005790600052602060002090600d020160005b506008015460018b81548110156100005790600052602060002090600d020160005b506009015460018c81548110156100005790600052602060002090600d020160005b50600a015460018d81548110156100005790600052602060002090600d020160005b50600b015460018e81548110156100005790600052602060002090600d020160005b50600c0154959c50939a50919850965094509250600160a060020a031690505b919395979092949650565b60006101a0604051908101604052806000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200160008152602001600081526020016000815260200150662386f26fc10000341015610df157610000565b662386f26fc10000841015610e0557610000565b43861015610e1257610000565b600160a060020a033316815260408101869052606081018590526000608082015260c081018490523460a0820152821515610e5357600060e0820152610e5b565b600160e08201525b43610100820152600180548082018083558281838015829011610f1257600d0281600d028360005260206000209182019101610f1291905b80821115610f0e578054600160a060020a031990811682556001820180548216905560006002830181905560038301819055600483018190556005830181905560068301819055600783018190556008830181905560098301819055600a8301819055600b830155600c820180549091169055600d01610e93565b5090565b5b50505091600052602060002090600d020160005b5082518154606060020a918202829004600160a060020a0319918216178355602085015160018401805491840284900491831691909117905560408501516002840155606085015160038401556080850151600484015560a0850151600584015560c0850151600684015560e0850151600784015561010085015160088401556101208501516009840155610140850151600a840155610160850151600b840155610180850151600c909301805493830292909204921691909117905591505b50949350505050565b600081831115611007575080820361100c5661100c565b508181035b5b92915050565b600060018281548110156100005790600052602060002090600d020160005b5060060154341461104257610000565b600060018381548110156100005790600052602060002090600d020160005b50600401541461107057610000565b3360018381548110156100005790600052602060002090600d020160005b5060010160006101000a815481600160a060020a030219169083606060020a908102040217905550600160018381548110156100005790600052602060002090600d020160005b50600401819055504360018381548110156100005790600052602060002090600d020160005b50600a015550805b919050565b60006000600060006000600060018781548110156100005790600052602060002090600d020160005b505460018054600160a060020a039092169189908110156100005790600052602060002090600d020160005b506001908101548154600160a060020a0390911691908a908110156100005790600052602060002090600d020160005b506002015460018a81548110156100005790600052602060002090600d020160005b506003015460018b81548110156100005790600052602060002090600d020160005b506005015460018c81548110156100005790600052602060002090600d020160005b50600601549550955095509550955095505b91939550919395565b445b9056' ;

// result.contracts.hashbet.interface
contract_interface = JSON.parse(`[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"entityStructs","outputs":[{"name":"creator","type":"address"},{"name":"buyer","type":"address"},{"name":"t_block","type":"uint256"},{"name":"t_diff","type":"uint256"},{"name":"status","type":"uint256"},{"name":"stake","type":"uint256"},{"name":"price","type":"uint256"},{"name":"direction","type":"uint256"},{"name":"b_open","type":"uint256"},{"name":"b_cancel","type":"uint256"},{"name":"b_buy","type":"uint256"},{"name":"b_resolve","type":"uint256"},{"name":"resolver","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"length","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"i","type":"uint256"}],"name":"cancel","outputs":[{"name":"rowNumber","type":"uint256"}],"payable":true,"type":"function"},{"constant":false,"inputs":[{"name":"i","type":"uint256"}],"name":"de_resolve","outputs":[{"name":"rowNumber","type":"uint256"}],"payable":true,"type":"function"},{"constant":false,"inputs":[{"name":"i","type":"uint256"},{"name":"diff","type":"uint256"}],"name":"ce_resolve","outputs":[{"name":"rowNumber","type":"uint256"}],"payable":true,"type":"function"},{"constant":true,"inputs":[{"name":"i","type":"uint256"}],"name":"betI","outputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"t_block","type":"uint256"},{"name":"t_diff","type":"uint256"},{"name":"price","type":"uint256"},{"name":"direction","type":"uint256"}],"name":"open","outputs":[{"name":"rowNumber","type":"uint256"}],"payable":true,"type":"function"},{"constant":false,"inputs":[{"name":"a","type":"uint256"},{"name":"b","type":"uint256"}],"name":"distance","outputs":[{"name":"c","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"i","type":"uint256"}],"name":"buy","outputs":[{"name":"rowNumber","type":"uint256"}],"payable":true,"type":"function"},{"constant":true,"inputs":[{"name":"i","type":"uint256"}],"name":"beti","outputs":[{"name":"","type":"address"},{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"diff_fn","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"}]`);
let tokenAddress = "REPLACE_WITH_ERC20_TOKEN_ADDRESS";
let walletAddress = "REPLACE_WITH_WALLET_ADDRESS";

// The minimum ABI to get ERC20 Token balance
let minABI = [
  // balanceOf
  {
    "constant":true,
    "inputs":[{"name":"_owner","type":"address"}],
    "name":"balanceOf",
    "outputs":[{"name":"balance","type":"uint256"}],
    "type":"function"
  },
  // decimals
  {
    "constant":true,
    "inputs":[],
    "name":"decimals",
    "outputs":[{"name":"","type":"uint8"}],
    "type":"function"
  },
{
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
];

// Get ERC20 Token contract instance
let token = web3.eth.contract(minABI).at('0x10da61e2b17bdf7a9c351d2d7debdcc562db9b2c');





function transform(beti,betI,i) {
    return {
        i         :                       i            ,
        creator   :                beti[0]             ,
        buyer     :                beti[1]             ,
        t_block   :                beti[2].toNumber()  ,
        t_diff    :                beti[3].toNumber()  ,
        stake     : web3.fromWei(  beti[4].toNumber() ),
        price     : web3.fromWei(  beti[5].toNumber() ),

        status    :                betI[0].toNumber()  ,
        dir       :                betI[1].toNumber()  ,

        b_open    :                betI[2].toNumber()  ,
        b_cancel  :                betI[3].toNumber()  ,
        b_buy     :                betI[4].toNumber()  ,
        b_resolve :                betI[5].toNumber()  ,
        resolver  :                betI[6]

    }
}

l_fn = function(error, result){
    if(!error)
        console.log(JSON.stringify(result));
    else
        console.error(error);
};

const s_open             = 0 ; 
const s_wait             = 1 ; 
const s_canceled         = 2 ; 
const s_resolved_creator = 3 ; 
const s_resolved_buyer   = 4 ; 


const promisify = (inner) =>
      new Promise((resolve, reject) =>
                  inner((err, res) => {
                      if (err) {reject (err);}
                      else     {resolve(res);}
                  })
                 );

Vue.component('demo-grid', {
  template: '#grid-template',
  props: {
    data: Array,
    columns: Array,
      filterKey: String,
      action: String,
   action2: String
  },
  data: function () {
    var sortOrders = {}
    this.columns.forEach(function (key) {
      sortOrders[key] = 1
    })
    return {
      sortKey: '',
      sortOrders: sortOrders
    }
  },
  computed: {
    filteredData: function () {
      var sortKey = this.sortKey
      var filterKey = this.filterKey && this.filterKey.toLowerCase()
      var order = this.sortOrders[sortKey] || 1
      var data = this.data
      if (filterKey) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().indexOf(filterKey) > -1
          })
        })
      }
      if (sortKey) {
        data = data.slice().sort(function (a, b) {
          a = a[sortKey]
          b = b[sortKey]
          return (a === b ? 0 : a > b ? 1 : -1) * order
        })
      }
      return data
    }
  },
  filters: {

      ethAddress: function (str) {
          return str.slice(0,6)+'...'+ str.slice(-4);
      },
      capitalize: function (str) {
          return str.charAt(0).toUpperCase() + str.slice(1)
      },
      direction: function (str,k) {
          if (k!='dir'){return str}

          return (k?'<i>up</i>':'down')
    }
  },
  methods: {
        log: function (s) { console.log(s);},
        buy: function (entry) {
            if ( confirm (
 //                `I agree to pay ${entry.price} ETH for the chance to recieve back my payment ( ${entry.price} ETH ) plus the stake ( ${entry.stake} ETH ) under the condition that at block number ${entry.t_block} the difficulty will be ${entry.dir?'below':'above'} ${entry.t_diff}.
 // **Target block number may vary by 10 blocks**
 // **0.25% of the total amount will be taken as a fee**
 //                `
                `I agree to pay ${entry.price} ETH.

If at target block number ${entry.t_block} the difficulty will be ${entry.dir?'below':'above'} ${entry.t_diff}, I will recieve back my payment (${entry.price} ETH) and the stake (${entry.stake} ETH).

 **Target block number may vary by 10 blocks**
 **0.25% of the total amount will be taken as a fee**
                `
                         ))
                 {
                c.buy(entry.i,{value:web3.toWei(entry.price),gas:500000, from:web3.eth.defaultAccount},l_fn)
            }
        },
        cancel: function (entry) {
            c.cancel(entry.i,{gas:500000, from:web3.eth.defaultAccount},l_fn)
        },
    sortBy: function (key) {
      this.sortKey = key
      this.sortOrders[key] = this.sortOrders[key] * -1
    }
  }
})
var bets = [];
var app = new Vue({
  el: '#app',
    methods: {
        log: function (s) { console.log('yoa');},

        burnChange: function () {
            console.log(this.burn);
        },
        doit: function () {
            console.log('yo');
        },
      changeA: function (){
          if (this.removeOrAdd =='Create') {
              this.removeOrAdd ='Destroy';
              this.classy='remove';
          }  else {
              this.removeOrAdd ='Create'
              this.classy='add';
          }
      },
        update_network : function (network) {
            this.adapter = network;
            switch(network) {
            case '1':
                this.adapter_name = 'Ether Mainnet';
                this.coin_abb = 'ETH';
                this.network_img = 'ether-m.png';
                this.contract_address = '0x10da61e2b17bdf7a9c351d2d7debdcc562db9b2c';
                c = MyContract.at(this.contract_address);
                break;
            case '3':
                this.adapter_name = 'Ropsten Testnet';
                this.network_img = 'ether-m.png';
                this.coin_abb = 'ETH';
                this.contract_address = '0xf8a4b1658f2a8165612305b9485e12d099bcd666';
                c = MyContract.at(this.contract_address);
                break;
            case '88':
                this.adapter_name = 'Ubiq';
                this.coin_abb = 'UBQ';
                this.network_img = 'ubiq-m.png';
                this.contract_address = '0x38b77c0e2fd4d8a9edfb7410d47de7a5e29f77a';
                c = MyContract.at(this.contract_address);
                break;
            case '200625':
                this.adapter_name = 'Akroma';
                this.coin_abb = 'AKR';
                this.network_img = 'akroma-m.png';
                this.contract_address = '0x38b77c0e2fd4d8a9edfb7410d47de7a5e29f77a';
                c = MyContract.at(this.contract_address);
                break;
            case '111':
                this.adapter_name = 'Ethereum Classic';
                this.coin_abb = 'ETC';
                this.network_img = 'classic-m.png';
                this.contract_address = '0x38b77c0e2fd4d8a9edfb7410d47de7a5e29f77a';
                c = MyContract.at(this.contract_address);
                break;
            default:
                this.adapter_name = 'Unknown';
                this.unknown_adapter = true;
            }



        },
        update_new: function (a,i) {
            debugger;
        },
        yoo: function () {
            web3.eth.getBlockNumber(function (e,r) {
                if (r) {
                    app.adapter_block = r ;
                    web3.eth.getBlock(r,function (e,r) {
                        if (r) app.adapter_diff = r.difficulty.toNumber() ;
                        app.yo();
                    });
                }
            });

            // Call balanceOf function
            // 
            token.balanceOf(web3.eth.accounts[0], function (error, balance) {
                // Get decimals
                token.decimals(function (error, decimals) {
                    // calculate a balance
                    balance = balance.div(10**decimals);
                    console.log(balance.toString());
                    this.balance = balance.toString();
                }.bind(this));
            }.bind(this));
            token.totalSupply( function (error, supply) {
                // Get decimals
                token.decimals(function (error, decimals) {
                    // calculate a supply
                    supply = supply.div(10**decimals);
                    console.log(supply.toString());
                    this.supply = supply.toString();
                }.bind(this));
            }.bind(this));
            this.wow ='kkk';
        },
        yo: function () {
            c.length(function (e,r) {
                var ps=[]
                while(app.gridData .length > 0) {app.gridData .pop();}
                while(app.gridData2.length > 0) {app.gridData2.pop();}
                while(app.gridData3.length > 0) {app.gridData3.pop();}
                for (i=0;i<r.toNumber();i++){
                    ps.push(promisify(
                        cb => c.beti(i,cb)
                    ))
                    ps.push(promisify(
                        cb => c.betI(i,cb)
                    ))
                }
                Promise.all(ps).then(function (a) {
                        bets = [];
                    for (i=0;i<a.length;i=i+2){
                        var b = transform(a[i],a[i+1],i/2);
                        bets.push(b);
                        switch (b.status){
                        case s_open             :

                            if (b.dir) { 
                                if (app.adapter_diff>b.t_diff){b.temp_result = 'creator';}
                                else                          {b.temp_result = 'buyer'  ;}
                            }else {
                                if (app.adapter_diff<b.t_diff){b.temp_result = 'creator';}
                                else                          {b.temp_result = 'buyer'  ;}
                            }
                            b.blocks_left    = b.t_block - app.adapter_block ;
                            if (b.blocks_left<0){
                                                               b.blocks_left    = 'passed' ;
                                                               b.temp_result = 'only cancel, no buy';
                            }
                            b.diff_distance =  app.adapter_diff - b.t_diff;
                            if (b.diff_distance>0) { b.diff_distance = '+'+b.diff_distance;}


                            app.gridData .push(b); break;
                        case s_wait             :
                            if (b.dir) { 
                                if (app.adapter_diff>b.t_diff){b.temp_result = 'creator';}
                                else                          {b.temp_result = 'buyer'  ;}
                            }else {
                                if (app.adapter_diff<b.t_diff){b.temp_result = 'creator';}
                                else                          {b.temp_result = 'buyer'  ;}
                            }
                            b.blocks_left    = b.t_block - app.adapter_block ;
                            if (b.blocks_left<0){
                                                               b.blocks_left    = 'ready' ;
                                                               b.temp_result = '*****';
                            }
                            b.diff_distance =  app.adapter_diff - b.t_diff;
                            if (b.diff_distance>0) { b.diff_distance = '+'+b.diff_distance;}
                            app.gridData2.push(b);

                            break;
                        case s_canceled         :                                        break;
                        case s_resolved_creator : b.result=0;     app.gridData3.push(b); break;
                        case s_resolved_buyer   : b.result=1;     app.gridData3.push(b); break;
                        }
                    }
                })
            });
        },
        open_new: function () {
            c.open(
                this.new_o.t_block,
                this.new_o.t_diff,
                web3.toWei(this.new_o.price),
                this.new_o.dir,
                {value:web3.toWei(this.new_o.stake),
                 gas:500000,
                 from:web3.eth.defaultAccount},l_fn)
        },
        resolve:function (o,diff) {
            c.resolve(o.i,o.t_block,diff, {gas:500000, from:web3.eth.defaultAccount},l_fn)
        }
    },
    events: {
        yo: function() {}
    },
  data: {
      adapter          : 0             ,
      unknown_adapter  : false         ,
      adapter_name     : '--loading--' ,
      coin_abb         : '***'         ,
      network_img      : ''            ,
      contract_address : ''            ,
      adapter_diff     : '******'      ,
      adapter_block    : '*****'       ,

      classy:'add',
      wow: 'yyy',
      result: '0',
      supply: '0',
      balance: '0',
      burn: '1',
      removeOrAdd: 'Create',
      new_o: {
          t_block :5,
          t_diff  :3,
          dir     :1,
          stake   :2,
          price   :4
      },

      open     :[],
      awaiting :[],
      closed   :[],

      searchQuery: '',

      action1:'Buy',
      gridColumns: [
          {n:'Blocks Left'             , k:'blocks_left'},
          {n:'Direction'               , k:'dir'},
          {n:'Target Difficulty'       , k:'t_diff'},
          {n:'Difficulty Distance'     , k:'diff_distance'},
          {n:'Temporary Result'        , k:'temp_result'},
          // {n:'Target Block'            , k:'t_block'},
          {n:'Creator'                 , k:'creator'},
          {n:'Stake'                   , k:'stake'},
          {n:'Price'                   , k:'price'}
      ],
    gridData: [],
      action2:'Cancel',
      gridColumns2: [
          {n:'Blocks Left'             , k:'blocks_left'},
          // {n:'Target Block'            , k:'t_block'},
          {n:'Direction'               , k:'dir'},
          // {n:'Difficulty Distance'     , k:'diff_distance'},
          {n:'Target Difficulty'       , k:'t_diff'},
          {n:'Temporary Result'        , k:'temp_result'},
          {n:'Creator'                 , k:'creator'},
          {n:'Stake'                   , k:'stake'},
          {n:'Price'                   , k:'price'},
          {n:'Buyer'                   , k:'buyer'}
      ],
    gridData2: [],
      gridColumns3: [
          {n:'Target Block'            , k:'t_block'},
          {n:'Direction'               , k:'dir'},
          {n:'Target Difficulty'       , k:'t_diff'},
          {n:'Result'                  , k:'result'},
          {n:'Creator'                 , k:'creator'},
          {n:'Stake'                   , k:'stake'},
          {n:'Price'                   , k:'price'},
          {n:'Buyer'                   , k:'buyer'}
      ],
      gridData3: [],
      guide: {
          power:100,
          time_delta:3,
          diff_up:10,
          stake:1,
          price:5
      }

  }
})




var c;
var MyContract;

window.addEventListener('load', async () => {
    // Modern dapp browsers...
    if (window.ethereum) {
        window.web3 = new Web3(ethereum);
        try {
            // Request account access if needed
            await ethereum.enable();
            // Acccounts now exposed
            MyContract = web3.eth.contract(contract_interface);

            web3.version.getNetwork((err, r) => {
                app.update_network(r);
                app.yoo();
            })
        } catch (error) {
            // User denied account access...
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);
        // Acccounts always exposed
            MyContract = web3.eth.contract(contract_interface);
            app.update_network(web3.version.network);
            app.yoo();
    }
    // Non-dapp browsers...
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
        // web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
        window.web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io"));
        app.update_network(web3.version.network);
        app.yoo();
    }
});

