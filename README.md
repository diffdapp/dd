## Temporary readme (this inforamtion is subject to change)

**Difficulty Dapp** <br>
**The right way to risk manage minning operations**

*Avaliable networks: ETH, ETC*

This is a new open-source Dapp for miners. Our goal is to create financial tools for miners to hedge their risks against the current and the future mining difficulty of the network 
(currently it only supports Ether and ETC, but we will add more coins soon) on one hand, and on the other hand, 
create tools for holders and other types of users to earn profit from providing liquidity for the contract. 

Basically, it's a basic P2P smart contract that operates like a derivative contract. At first, we want to integrate traditional easy to use tools - like 
"the Black and Schultz pricing model calculator" and other options strategies. 

We will be adding intoductory information about thr project to this repository as we progress (including our goals, values, more economical incentives, 
progress of the project and so on).

We would love to hear your feedback on this and any contributions are more than welcome.